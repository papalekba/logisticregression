# -*- coding: utf-8 -*-
"""
Created on Tue Sep 17 23:22:21 2019

@author: rstbb
"""
import numpy as np

def sigmoid(h):
    return 1/(1 + np.exp(-h))


def cross_entropy(y, p_hat):
    return -(1/len(y))*np.sum(y*np.log(p_hat) + (1 - y)*np.log(1 - p_hat))


def accuracy(y, y_hat):
    return np.mean(y == y_hat)         
