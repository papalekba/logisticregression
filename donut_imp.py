# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 08:15:42 2019

@author: rstbb
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from matplotlib.colors import ListedColormap
from utils import accuracy
# from matplotlib.patches import Ellipse # Need to use this for my decision bd plot
from LogisticRegression import LogisticRegression


df = pd.read_csv('cloud/donut.csv', sep=' ', header=None, names = ['v1', 'v2', 'vy'])



cmap_bold = ListedColormap(["#FF0000", "#0000FF"])
cmap_light = ListedColormap(["#FFCCCC", "#BBCCFF"])


def main():
    donut = df[['v1', 'v2']]
    donut['vc'] = np.sqrt((donut.v1)**2) + np.sqrt((donut.v2)**2)
    
    X = donut.to_numpy()
    

    y = df['vy'].to_numpy()
    
    
    plt.figure()
    plt.scatter(X[:,0], X[:,1], c = y, alpha = 0.5)
    plt.show()
    
    log_reg = LogisticRegression()
    log_reg.fit(X, y, eta = 1e-1, epochs = 2e3, show_curve = True)
    y_hat = log_reg.predict(X)
    
    print(type(y_hat))
    
    print(f"Training Accuracy: {accuracy(y, y_hat):0.4f}")
    
    
    x1 = np.linspace(X[:,0].min() - 1, X[:,0].max() + 1, 1000)
    x2 = np.sqrt((log_reg.b/log_reg.w[2])**2 - x1**2)
    x3 = -(np.sqrt((log_reg.b/log_reg.w[2])**2 - x1**2))
    
    plt.figure()
    plt.scatter(X[:,0], X[:,1], c = y, alpha = 0.5)
    plt.plot(x1, x2, color = "#000000", linewidth = 2)
    plt.plot(x1, x3, color = "#000000", linewidth = 2)         
    plt.show()
    
    
    """
    xx1, xx2 = np.meshgrid(x1, x1)
    Z = log_reg.predict(np.c_[xx1.ravel(),xx2.ravel()]).reshape(*xx1.shape)
    
    plt.figure()
    plt.pcolormesh(xx1, xx2, Z, cmap = cmap_light)
    plt.scatter(X[:,0], X[:,1], c = y, cmap = cmap_bold)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    plt.show()
    """

if __name__ == "__main__":
    main()