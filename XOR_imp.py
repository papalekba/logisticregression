# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 08:15:42 2019

@author: rstbb
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from utils import accuracy
# from matplotlib.patches import Ellipse # Need to use this for my decision bd plot
from LogisticRegression import LogisticRegression


df = pd.read_csv('cloud/XOR.csv')




def main():
    xor = df[['x1', 'x2']]
    xor['x1x2'] = xor.x1*xor.x2
    
    X = xor.to_numpy()
    

    y = df['y'].to_numpy()
    
    
    plt.figure()
    plt.scatter(X[:,0], X[:,1], c = y, alpha = 0.5)
    plt.show()
    
    log_reg = LogisticRegression()
    log_reg.fit(X, y, eta = 1e-1, epochs = 2e3, show_curve = True)
    y_hat = log_reg.predict(X)
    
    print(type(y_hat))
    
    print(f"Training Accuracy: {accuracy(y, y_hat):0.4f}")
    
    
    x1 = np.linspace(X[:,0].min() - 1, X[:,0].max() + 1, 1000)
    x2 = (-(log_reg.b + x1*log_reg.w[0]))/(log_reg.w[1] + x1*log_reg.w[2])
    
    plt.figure()
    plt.scatter(X[:,0], X[:,1], c = y, alpha = 0.5)
    plt.plot(x1, x2, color = "#000000", linewidth = 2)         
    plt.show()
    

if __name__ == "__main__":
    main()